import { checkUpperCase } from "./checkUpperCase.js";
import { describe, it } from "@jest/globals";

describe("checkUpperCase", () => {
  it("Should return true for string with first letter in A-Z range", () => {
    expect(checkUpperCase("True")).toBeTruthy();
    expect(checkUpperCase("Display")).toBeTruthy();
    expect(checkUpperCase("Flex")).toBeTruthy();
  });

  it("Should return false for string with first letter outside A-Z range", () => {
    expect(checkUpperCase("true")).toBeFalsy();
    expect(checkUpperCase("Łeb")).toBeFalsy();
    expect(checkUpperCase("display")).toBeFalsy();
  });

  it("Should return false for not a string values", () => {
    expect(checkUpperCase(1)).toBeFalsy();
    expect(checkUpperCase(undefined)).toBeFalsy();
    expect(checkUpperCase(null)).toBeFalsy();
  });

  it("Should return false for empty string", () => {
    expect(checkUpperCase(" ")).toBeFalsy();
    expect(checkUpperCase("")).toBeFalsy();
  });
});

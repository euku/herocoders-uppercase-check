/**
 * Function that determines if a string
 * starts with an upper-case letter A-Z and returns true or false
 * @param   {string} str String
 * @returns {boolean}    true if first char belongs to A-Z range, otherwise false
 */

export function checkUpperCase(str) {
  return typeof str === "string" && /^[A-Z]/.test(str);
}

console.log(
  "Apple is starts with an upper-case letter: ",
  checkUpperCase("Apple")
);

console.log(
  "pineapple is starts with an upper-case letter: ",
  checkUpperCase("pineapple")
);
